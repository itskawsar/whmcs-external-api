<?php

/* 
 * WHMCSExternalAPI
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license,
 * visit http://creativecommons.org/licenses/by-nc-nd/4.0/deed.en_GB.
 * Filename helpers.php
 * @author Promise
 */

/**
 * Converts a date to US date.
 * @param date $date any date.
 * @return date the date in US format.
 */
function toUSDate($date) {
    list($day, $month, $year) = sscanf($date, "%d/%d/%d");
    $usdate = "$day-$month-$year";
    return $usdate;
}
/* 
 * End of file helpers.php
 */

