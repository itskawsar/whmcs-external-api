<?php

/*
 * WHMCSExternalAPI v1.0
 * This work is licensed under the Creative Commons 
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, 
 * visit http://creativecommons.org/licenses/by-nc-nd/4.0/deed.en_GB.
 * Filename api.php
 * @author Promise Ekoriko <pekoriko@gx5.co.uk>
 */

//defined('BASEPATH') OR exit('No direct script access allowed');

class WHMCSAPI {

    /**
     * Connection url.
     * @var string concactenated string.
     */
    var $conurl = "";

    /**
     * Postfields required for WHMCS api.
     * @var array contains multiple values.
     */
    var $postfield = array();

    /**
     * Response type of of the API.
     * @var string Can be any of "xml" or "json".
     */
    var $datatype = "json";

    /**
     * Constructor for a new WHMCSAPI object.
     * @global string $conurl.
     * @param string $url WHMCS API url.
     * @param string $name WHMCS API admin username.
     * @param string $password WHMCS API admin password.
     * @param string $datatype Can be any of "xml" or "json".
     */
    function __construct($url, $name, $password, $datatype = "json") {
        global $conurl;
        $u = $url . "/includes/api.php?";
        echo $conurl;
        $postfield["username"] = $name;
        $postfield["password"] = md5($password);
        $postfield["responsetype"] = $datatype;
        $GLOBALS['postfield'] = $postfield;
        $GLOBALS['conurl'] = $u;
    }

    /**
     * Function to add an order to WHMCS.
     * @global array $postfield Postfields array.
     * @param int $cid Clients ID.
     * @param int $pid Product ID.
     * @param array $customfields
     * @param boolean $noemail
     * @param string $paymentmethod Payment method name. You can get this from 
     * WHMCS
     * @param string $billingcycle Billing cycle.
     * @return array Response type.
     */
    function addOrder($cid, $pid, $customfields, $noemail, $paymentmethod, $billingcycle = 'monthly') {
        global $postfield;
        $command = "addorder";
        $postfield["action"] = $command;
        $postfield['clientid'] = $cid;
        $postfield['pid'] = $pid;
        $postfield['noemail'] = $noemail;
        $postfield['billingcycle'] = $billingcycle;
        $postfield['customfields'] = base64_encode(serialize($customfields));
        $postfield['paymentmethod'] = $paymentmethod;
        return $this->execute();
    }

    /**
     * Function to add a client to WHMCS.
     * @global array $postfield Postfields array.
     * @param string $firstname Clients First Name
     * @param string $lastname Clients Second Name
     * @param string $email Clients Email.
     * @param string $phonenumber Clients Phone Number
     * @param array $customfields Custom fields if any.
     * @param string $companyname Clients Company Name.
     * @param string $address1 Clients Address1
     * @param string $city Clients City.
     * @param string $state Clients State.
     * @param string $postcode Clients Postcode.
     * @param string $country Clients Country. Use ISO "Prefix" code.
     * @param string $password Clients Password in plaintext. Will be encrypted.
     * @param string $currency Clients Default Currency.
     * @param string $noemail Set to false to suppress email behaviour.
     * @param string $skipvalidation Set to "true" to skip validation.
     * @return array Response type.
     */
    function addClient($firstname, $lastname, $email, $phonenumber, $customfields, $companyname = null, $address1 = "AAA", $city = "AAA", $state = "AAA", $postcode = "AAA", $country = "GB", $password = null, $currency = "1", $noemail = "true", $skipvalidation = "true") {
        global $postfield;
        $command = "addclient";
        $postfield["action"] = $command;
        $postfield["firstname"] = $firstname;
        $postfield["lastname"] = $lastname;
        $postfield["email"] = $email;
        $postfield["phonenumber"] = $phonenumber;
        $postfield["customfields"] = base64_encode(serialize($customfields));
        $postfield["address1"] = $address1;
        $postfield["city"] = $city;
        $postfield["state"] = $state;
        $postfield["postcode"] = $postcode;
        $postfield["country"] = $country;
        $postfield["password2"] = base64_encode(sha1(rand(1231312, 134235346454)));
        $postfield["noemail"] = $noemail;
        if (isset($companyname)) {
            $postfield["companyname"] = $companyname;
        }
        $postfield["currency"] = $currency;
        $postfield["skipvalidation"] = $skipvalidation;
        return $this->execute();
    }

    /**
     * Updates a user with given info.
     * @author Vincent Akoto<vincent.akoto@gmail.com>
     * @global array $postfield
     * @param array $info Information to be updated. id is a must.
     */
    function updateClient($info) {
        global $postfield;
        $command = "updateclient";
        $postfield["action"] = $command;
        $postfield["clientid"] = $info["id"];
        if (isset($info["firstname"])) {
            $postfield["firstname"] = $info["firstname"];
        }
        if (isset($info["lastname"])) {
            $postfield["lastname"] = $info["lastname"];
        }
        if (isset($info["companyname"])) {
            $postfield["companyname"] = $info["companyname"];
        }
        if (isset($info["email"])) {
            $postfield["email"] = $info["email"];
        }
        if (isset($info["address1"])) {
            $postfield["address1"] = $info["address1"];
        }
        if (isset($info["address2"])) {
            $postfield["address2"] = $info["address2"];
        }
        if (isset($info["city"])) {
            $postfield["city"] = $info["city"];
        }
        if (isset($info["state"])) {
            $postfield["state"] = $info["state"];
        }
        if (isset($info["postcode"])) {
            $postfield["postcode"] = $info["postcode"];
        }
        if (isset($info["country"])) {
            $postfield["country"] = $info["county"];
        }
        if (isset($info["phonenumber"])) {
            $postfield["phonenumber"] = $info["phonenumber"];
        }
        if (isset($info["password2"])) {
            $postfield["password2"] = $info["password2"];
        }
        if (isset($info["credit"])) {
            $postfield["credit"] = $info["credit"];
        }
        if (isset($info["taxexempt"])) {
            $postfield["taxexempt"] = $info["taxexempt"];
        }
        if (isset($info["notes"])) {
            $postfield["notes"] = $info["notes"];
        }
        if (isset($info["cardtype"])) {
            $postfield["cardtype"] = $info["cardtype"];
        }
        if (isset($info["cardnum"])) {
            $postfield["cardnum"] = $info["cardnum"];
        }
        if (isset($info["expdate"])) {
            $postfield["expdate"] = $info["expdate"];
        }
        if (isset($info["startdate"])) {
            $postfield["startdate"] = $info["startdate"];
        }
        if (isset($info["issuenumber"])) {
            $postfield["issuenumber"] = $info["issuenumber"];
        }
        if (isset($info["clearcreditcard"])) {
            $postfield["clearcreditcard"] = $info["clearcreditcard"];
        }
        if (isset($info["language"])) {
            $postfield["lsnguage"] = $info["language"];
        }
        if (isset($info["customfields"])) {
            $postfield["customfields"] = base64_encode(serialize($info["customfields"]));
        }
        if (isset($info["status"])) {
            $postfield["status"] = $info["status"];
        }
        if (isset($info["taxexempt"])) {
            $postfield["taxexempt"] = $info["taxexempt"];
        }
        if (isset($info["latefeeoveride"])) {
            $postfield["latefeeoveride"] = $info["latefeeoveride"];
        }
        if (isset($info["overideduenotices"])) {
            $postfield["overideduenotices"] = $info["overideduenotices"];
        }
        if (isset($info["separateinvoices"])) {
            $postfield["separateinvoices"] = $info["separateinvoices"];
        }
        if (isset($info["disableautocc"])) {
            $postfield["disableautocc"] = $info["disableautocc"];
        }
        return $this->execute();
    }
    /**
     * Delete a client with the given ID.
     * @global array $postfield.
     * @param int $id The ID of the client you want to delete.
     * @return mixed.
     */
    function deleteClient($id) {
        global $postfield;
        $command = "deleteclient";
        $postfield["action"] = $command;
        $postfield["clientid"] = $id;
        return $this->execute();
    }
    /**
     * Adds Contact to a WHMCS account.
     * @author Vincent Akoto <vincent.akoto@gmail.com>
     * @global array $postfield
     * @param array $info.
     * @return mixed.
     */
    function addContact($info) {
        global $postfield;
        $command = "addcontact";
        $client = $info["clientid"];
        $postfield["action"] = $command;
        $postfield["clientid"] = $client;
        if (isset($info["firstname"])) {
            $postfield["firstname"] = $info["firstname"];
        }
        if (isset($info["lastname"])) {
            $postfield["lastname"] = $info["lastname"];
        }
        if (isset($info["companyname"])) {
            $postfield["companyname"] = $info["companyname"];
        }
        if (isset($info["email"])) {
            $postfield["email"] = $info["email"];
        }
        if (isset($info["address1"])) {
            $postfield["address1"] = $info["address1"];
        }
        if (isset($info["address2"])) {
            $postfield["address2"] = $info["address2"];
        }
        if (isset($info["city"])) {
            $postfield["city"] = $info["city"];
        }
        if (isset($info["state"])) {
            $postfield["state"] = $info["state"];
        }
        if (isset($info["postecode"])) {
            $postfield["postecode"] = $info["postecode"];
        }
        if (isset($info["country"])) {
            $postfield["country"] = $info["country"];
        }
        if (isset($info["phonenumber"])) {
            $postfield["phonenumber"] = $info["phonenumber"];
        }
        if (isset($info["password2"])) {
            $postfield["password2"] = $info["password2"];
        }
        if (isset($info["permissions"])) {
            $postfield["permissions"] = $info["permissions"];
        }
        if (isset($info["generalemails"])) {
            $postfield["generalemails"] = $info["generalemails"];
        }
        if (isset($info["productemails"])) {
            $postfield["productemails"] = $info["productemails"];
        }
        if (isset($info["domainemails"])) {
            $postfield["domainemails"] = $info["domainemails"];
        }
        if (isset($info["invoiceemails"])) {
            $postfield["invoiceemails"] = $info["invoiceemails"];
        }
        if (isset($info["supportemails"])) {
            $postfield["supportemails"] = $info["supportemails"];
        }
        return $this->execute();
    }
    /**
     * Allows you to send an email.
     * @global array $postfield Postfields array.
     * @param array $params Config and setup fields.
     */
    function sendEmail($params) {
        global $postfield;
        $command = "sendemail";
        $postfield["action"] = $command;
        $postfield["messagename"] = $params["messagename"];
        $postfield["id"] = $params["id"];
        $postfield["customtype"] = $params["type"];
        $postfield["customsubject"] = $params["subject"];
        $postfield["custommessage"] = $params["message"];
        $postfield["customvars"] = base64_encode(serialize($params["variables"]));
        $this->execute();
    }

    /**
     * Searches and retrieves details of a client.
     * @global array $postfield Postfields array.
     * @param string $email Clients email.
     * @param int $clientid Clients id.
     * @return array Clients details if found.
     */
    function getClientsDetails($email, $clientid = null) {
        global $postfield;
        $command = "getclientsdetails";
        $postfield["action"] = $command;
        $postfield["stats"] = true;
        if (isset($clientid)) {
            $postfield["clientid"] = $clientid;
        } else {
            $postfield["email"] = $email;
        }
        $client = $this->execute();
        if ($client == null) {
            return "No client found";
        }
        return $client;
    }

    /**
     * Parses an XML response
     * @param xml $rawxml The response in XML.
     * @return xml returns a well formed XML.
     */
    private function whmcsapi_xml_parser($rawxml) {
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $rawxml, $vals, $index);
        xml_parser_free($xml_parser);
        $params = array();
        $level = array();
        $alreadyused = array();
        $x = 0;
        foreach ($vals as $xml_elem) {
            if ($xml_elem['type'] == 'open') {
                if (in_array($xml_elem['tag'], $alreadyused)) {
                    $x++;
                    $xml_elem['tag'] = $xml_elem['tag'] . $x;
                }
                $level[$xml_elem['level']] = $xml_elem['tag'];
                $alreadyused[] = $xml_elem['tag'];
            }
            if ($xml_elem['type'] == 'complete') {
                $start_level = 1;
                $php_stmt = '$params';
                while ($start_level < $xml_elem['level']) {
                    $php_stmt .= '[$level[' . $start_level . ']]';
                    $start_level++;
                }
                $php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
                @eval($php_stmt);
            }
        }
        return($params);
    }

    /**
     * Executes API.
     * @global array $postfield Postfields array.
     * @global string $conurl Connection url.
     * @return multiple Depends on your "responsetype" setting. Default is JSON.
     */
    private function execute() {
        global $postfield, $conurl;
        $conurl = $GLOBALS['conurl'];
        $postfield = $GLOBALS['postfield'];
        //var_dump($postfield);
        //echo $conurl;
        $query_string = "";
        foreach ($postfield as $k => $v):
            $query_string .= "$k=" . urlencode($v) . "&";
        endforeach;
        //echo $query_string;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $conurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        if (curl_error($ch)):
            die("Connection Error: " . curl_errno($ch) . ' - ' . curl_error($ch));
        endif;
        curl_close($ch);
        if ($this->datatype == "xml") {
            if (curl_error($ch) || !$xml) {
                $xml = '<whmcsapi><result>error</result>' .
                        '<message>Connection Error</message><curlerror>' .
                        curl_errno($ch) . ' - ' . curl_error($ch) . '</curle'
                        . 'rror></whmcsapi>';
            }
        }
        $arr = json_decode($data);
        //print_r($arr);
        return $arr;
    }

}

/* 
 * End of file api.php
 */
