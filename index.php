<?php

/*
 * WHMCSExternalAPI
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license,
 * visit http://creativecommons.org/licenses/by-nc-nd/4.0/deed.en_GB.
 * Filename index.php
 * @author Promise
 */
require 'includes/WHMCSAPI.php';
include 'helpers/helper.php';

$whmcsAPI = new WHMCSAPI("https://urltoyourwhmcsinstallation", "adminUserName", "adminPassword");
/**
 * Get ClientsDetailsTest
 */
$client = $whmcsAPI->getClientsDetails("emailtosearch");
print_r($client);
$cid = $client->client->id; //Get the ID of the returned client.
/**
 * Add order test   
 */
$whmcsAPI->addOrder($cid, 16, $customfields=array(), "true", "mailin");
/* 
 * End of file index.php
 */

