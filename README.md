#WHMCS External API

This project contains a set of External WHMCS Commands that can be easily used
with any PHP project.

##Current API Support
- Add Order
- Add Clients
- Get Clients Details
- Get Clients
- Send Email

###How to Use
You will need to have WHMCS from this point on. You can purchase 
WHMCS [here](http://www.whmcs.com/members/aff.php?aff=11170).

1. Place the contents of includes into an "includes" directory or any directory
of your choosing.
2. Go to your WHMCS application and create an API user.
3. Go to your WHMCS security settings and whitelist your IP address.
4. The index file contains an example implementation of the API. Open it an edit
the contents. Replace names and paths appropriately.
5. Create a WHMCSAPI object and begin communicating with your application.

####Help and Support
Each function is properly documented. If you find however that you need 
specialist support, then you can contact me at support@gx5.co.uk.
If you want to contribute to the project, please email me at pekoriko@gx5.co.uk.

Learn more about the WHMCS API [here](http://docs.whmcs.com/API).

###Legal Notice
This program contains some contents from [WHMCS.com](http://whmcs.com) and 
as such we are acknowledging their work.